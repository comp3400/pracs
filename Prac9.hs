{- Prac 9: Logic programming with LogicT

Code and exercises in this prac are derived from paper
/Backtracking, Interleaving and Terminating Monad Transformers/
(Kiselyov et al, 2005) [1] and the /logict/ library [2].

[1] http://okmij.org/ftp/papers/LogicT.pdf
[2] https://hackage.haskell.org/package/logict

I recommend reading the paper for a deeper understanding of the
motivation and implementation of these abstractions, and the laws.

-}
{-# OPTIONS_GHC -Wall -Wno-unused-imports #-}

{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE RankNTypes #-}

module Prac9 where

import Prelude
  ( ($), (.), const, id, flip, error
  , Int, (+), (-), even, odd, mod
  )


import Data.Bool
import Data.Eq
import Data.Foldable (Foldable(..))
import Data.Functor
import Data.Functor.Identity
import Data.Maybe
import Data.Ord
import Data.Semigroup (Semigroup(..))
import Data.Monoid (Monoid(..))
import Data.Tuple
import Control.Applicative (Applicative(..))
import Control.Monad (Monad(..), (=<<))


{- EXERCISE: Church-encoded list -}

newtype List a = List
  { unList :: forall r. (a -> r -> r) -> r -> r }

nil :: List a
nil = error "todo"

cons :: a -> List a -> List a
cons = error "todo"

foldRight :: (a -> b -> b) -> b -> List a -> b
foldRight = error "todo"

instance Semigroup (List a) where
  (<>) :: List a -> List a -> List a
  (<>) = error "todo"

instance Functor List where
  fmap :: (a -> b) -> List a -> List b
  fmap = error "todo"

instance Applicative List where
  pure :: a -> List a
  pure = error "todo"

  (<*>) :: List (a -> b) -> List a -> List b
  (<*>) = error "todo"

instance Monad List where
  (>>=) :: List a -> (a -> List b) -> List b
  (>>=) = error "todo"


{- EXERCISE: Alternative and MonadPlus -}

class (Applicative k) => Alternative k where
  -- | failure
  empty :: k a

  -- | choice
  (<|>) :: k a -> k a -> k a

guard :: (Alternative k) => Bool -> k ()
guard = error "todo"

-- | Like Alternative, with additional laws related to Monad.
--
class (Alternative k, Monad k) => MonadPlus k where
  mzero :: k a
  mzero = error "todo: mzero default implementation"

  mplus :: k a -> k a -> k a
  mplus = error "todo: mplus default implementation"


instance Alternative List where
  empty = error "todo"
  (<|>) = error "todo"

instance MonadPlus List where
  -- use default implementations


{- EXERCISE: LogicT -}

-- | This is the Church-encoded list transformer.  We will equip
--   it with special behaviour for logic programming later.
--
newtype LogicT k a = LogicT
  { unLogicT :: forall r. (a -> k r -> k r) -> k r -> k r }

instance Functor (LogicT k) where
  fmap :: (a -> b) -> LogicT k a -> LogicT k b
  fmap = error "todo"

instance Applicative (LogicT k) where
  pure :: a -> LogicT k a
  pure = error "todo"

  (<*>) :: LogicT k (a -> b) -> LogicT k a -> LogicT k b
  (<*>) = error "todo"

instance Monad (LogicT k) where
  (>>=) :: LogicT k a -> (a -> LogicT k b) -> LogicT k b
  (>>=) = error "todo"

instance Alternative (LogicT k) where
  empty :: LogicT k a
  empty = error "todo"

  (<|>) :: LogicT k a -> LogicT k a -> LogicT k a
  (<|>) = error "todo"

instance MonadPlus (LogicT k) where
  -- use default implementations

{- EXERCISE: Foldable LogicT -}

instance (Applicative k, Foldable k) => Foldable (LogicT k) where
  foldMap :: (Monoid m) => (a -> m) -> LogicT k a -> m
  foldMap = error "todo"

asum :: (Foldable t, Alternative k) => t (k a) -> k a
asum = error "todo"


{- EXERCISE: LogicT combinators -}

lift :: (Monad k) => k a -> LogicT k a
lift = error "todo"

reflect :: (Alternative k) => Maybe (a, k a) -> k a
reflect = error "todo"

msplit :: (Monad k) => LogicT k a -> LogicT k (Maybe (a, LogicT k a))
msplit = error "todo"

-- | Fair disjunction
interleave :: (Monad k) => LogicT k a -> LogicT k a -> LogicT k a
interleave = error "todo"

-- | Fair conjunction
(>>-) :: (Monad k) => LogicT k a -> (a -> LogicT k b) -> LogicT k b
(>>-) = error "todo"

-- | Logical conditional
ifte
  :: (Monad k)
  => LogicT k a         -- ^ test
  -> (a -> LogicT k b)  -- ^ success branch
  -> LogicT k b         -- ^ failure branch
  -> LogicT k b
ifte = error "todo"

-- | Pruning; select one result
once :: (Monad k) => LogicT k a -> LogicT k a
once = error "todo"

-- | Logical negation
lnot :: (Monad k) => LogicT k a -> LogicT k ()
lnot = error "todo"



{- EXERCISE: observing results -}

-- | Extract first result
observeT :: (Applicative k) => LogicT k a -> k (Maybe a)
observeT = error "todo"

-- | Extract all results
observeAllT :: (Applicative k) => LogicT k a -> k [a]
observeAllT = error "todo"

-- | Extract up to /n/ results
observeManyT :: (Monad k) => Int -> LogicT k a -> k [a]
observeManyT = error "todo"


{- EXERCISE: Logic -}

type Logic = LogicT Identity

observe :: Logic a -> Maybe a
observe = error "todo"

observeAll :: Logic a -> [a]
observeAll = error "todo"

observeMany :: Int -> Logic a -> [a]
observeMany = error "todo"


{- EXERCISE: building logic computations -}

odds :: LogicT k Int
odds = error "todo"

oddsPlus :: Int -> LogicT k Int
oddsPlus = error "todo"

zeroAndOne :: LogicT k Int
zeroAndOne = error "todo"

oddsAndEvensNaïve :: LogicT k Int
oddsAndEvensNaïve = error "todo"

oddsAndEvensFair :: (Monad k) => LogicT k Int
oddsAndEvensFair = error "todo"

-- | Return even numbers from the input
--
-- What happens if you apply to oddsAndEvensFair?
-- What happens if you apply to oddsAndEvensNaïve?
--
evensFrom :: LogicT k Int -> LogicT k Int
evensFrom = error "todo"

-- | Generate ints from 1..n
iota :: (Alternative k) => Int -> k Int
iota = error "todo"

oddPrimes :: (Monad k) => LogicT k Int
oddPrimes = error "todo"

-- use 'oddPrimes' and @pure 2@
primes :: (Monad k) => LogicT k Int
primes = error "todo"

-- use 'primes'
evenPrimes :: (Monad k) => LogicT k Int
evenPrimes = error "todo"
