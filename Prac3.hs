{-# LANGUAGE NoImplicitPrelude #-}

module Prac3 where

import Data.String
import Data.Bool
import Data.Char (Char)
import Text.Show

import Prelude (error)


{- DATA TYPES AND CLASSES -}

data Optional a = Empty | Full a
  deriving (Show)

data List a = Nil | Cons a (List a)
  deriving (Show)

data Pair a b = Pair a b

data Or a b = IsA a | IsB b

data NonEmptyList a = NonEmptyList a (List a)

data Tree a = Tree a (List (Tree a))

data Vector6 a = Vector6 a a a a a a

data ParseResult x =
  ParseError String | ParseSuccess x String
data Parser x = Parser (String -> ParseResult x)

class Functor f where
  fmap :: (a -> b) -> f a -> f b


{- EXERCISE 1: Functor instances #-}

instance Functor Optional where
  fmap = error "todo"

instance Functor Vector6 where
  fmap = error "todo"

instance Functor List where
  fmap = error "todo"

instance Functor NonEmptyList where
  fmap = error "todo"

instance Functor Tree where
  fmap = error "todo"

instance Functor (Pair a) where
  fmap = error "todo"

instance Functor (Or a) where
  fmap = error "todo"

instance Functor Parser where
  fmap = error "todo"


{- EXERCISE 2: Functor derived operations #-}

(<$) :: (Functor f) => b -> f a -> f b
(<$) = error "todo"
infixl 4 <$

($>) :: (Functor f) => f a -> b -> f b
($>) = error "todo"
infixl 4 $>

(<$>) :: (Functor f) => (a -> b) -> f a -> f b
(<$>) = error "todo"
infixl 4 <$>

void :: (Functor f) => f a -> f ()
void = error "todo"


{- EXERCISE 3: liftF2 -}

-- Can this function be implemented?  Why / why not?
liftF2 :: (Functor f) => (a -> b -> c) -> f a -> f b -> f c
liftF2 = error "todo"


{- EXERCISE 4: liftA2 -}

class (Functor f) => Applicative f where
  (<*>) :: f (a -> b) -> f a -> f b
  pure :: a -> f a
infixl 4 <*>

liftA2 :: (Applicative f) => (a -> b -> c) -> f a -> f b -> f c
liftA2 = error "todo"


{- EXERCIES 5: Applicative instances -}

instance Applicative Optional where
  (<*>) = error "todo"
  pure = error "todo"

instance Applicative Vector6 where
  (<*>) = error "todo"
  pure = error "todo"

instance Applicative List where
  (<*>) = error "todo"
  pure = error "todo"

instance Applicative NonEmptyList where
  (<*>) = error "todo"
  pure = error "todo"

instance Applicative (Or a) where
  (<*>) = error "todo"
  pure = error "todo"

-- Can this instance be implemented?  Why / why not?
instance Applicative (Pair a) where
  (<*>) = error "todo"
  pure = error "todo"

instance Applicative Parser where
  (<*>) = error "todo"
  pure = error "todo"


{- EXERCISE 6: Parser functions #-}

runParser :: Parser a -> String -> ParseResult a
runParser (Parser f) = f

-- | Succeeds if string is non-empty and next Char satisfies
--   the predicate
satisfy :: (Char -> Bool) -> Parser Char
satisfy = error "todo"

-- | Always succeeds if the string is non-empty
anyChar :: Parser Char
anyChar = error "todo"

-- | Parse 't' or 'f', otherwise fail.
tOrF :: Parser Char
tOrF = error "todo"

-- | Parse a digit ('0' .. '9')
digit :: Parser Char
digit = error "todo"

-- | Parse a data format that consists of two characters.
--   If the first character is 'd', the next character must be a digit.
--   If the first character is 'b', the next character must be
--     either 't' or 'f'.
--   Return the second character.
parseFormat :: Parser Char
parseFormat = error "todo"


{- EXERCISE 7: Monad instances -}

class Applicative m => Monad m where
  (>>=) :: m a -> (a -> m b) -> m b

instance Monad Parser where
  (>>=) = error "todo"

-- Now you can use (>>=) to implement 'parseFormat'

instance Monad Optional where
  (>>=) = error "todo"

instance Monad List where
  (>>=) = error "todo"

instance Monad (Or a) where
  (>>=) = error "todo"

-- Can you implement this instance?
-- What are the challenges or concerns?
instance Monad Vector6 where
  (>>=) = error "todo"
