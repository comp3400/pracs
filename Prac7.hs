{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE RankNTypes #-}

{-# OPTIONS_GHC -Wall -Wno-unused-imports #-}

module Prac7 where

import Prelude ((.), ($), const, id, error, Integer)

import Control.Applicative (Applicative(..))
import Data.Bool
import Data.Eq
import Data.Functor
import Data.List (foldr)
import Data.Maybe
import Data.Ord
import Data.String (String)
import Text.Show


{- PRELUDE -}

data Identity a = Identity { getIdentity :: a }
  deriving (Show)

instance Functor Identity where
  fmap :: (a -> b) -> Identity a -> Identity b
  fmap f (Identity a) = Identity (f a)

instance Applicative Identity where
  pure :: a -> Identity a
  pure = Identity

  (<*>) :: Identity (a -> b) -> Identity a -> Identity b
  Identity f <*> Identity a = Identity (f a)

data Const c a = Const { getConst :: c }

instance Functor (Const c) where
  fmap :: (a -> b) -> Const c a -> Const c b
  fmap _f (Const c) = Const c

instance (Monoid c) => Applicative (Const c) where
  pure :: a -> Const c a
  pure _a = Const mempty

  (<*>) :: Const c (a -> b) -> Const c a -> Const c b
  Const c1 <*> Const c2 = Const (c1 <> c2)


class Semigroup (a :: *) where
  (<>) :: a -> a -> a

class (Semigroup a) => Monoid a where
  mempty :: a

instance Semigroup [a] where
  l1 <> l2 = foldr (:) l2 l1

instance Monoid [a] where
  mempty = []


{- EXERCISE: First -}

-- | A wrapper around 'Maybe' with left-biased Semigroup instance
-- Left-biased is taken to mean "select the the first Just value, starting from the left"
data First a = First { getFirst :: Maybe a }
  deriving (Eq, Ord, Show)

instance Semigroup (First a) where
  (<>) = error "todo"

instance Monoid (First a) where
  mempty = error "todo"


{- EXERCISE: Foldable and Traversable -}

class Foldable t where
  foldMap
    :: (Monoid m)
    => (a -> m) -> t a -> m

instance Foldable [] where
  foldMap :: (Monoid m) => (a -> m) -> [a] -> m
  foldMap = error "todo"

instance Foldable Maybe where
  foldMap :: (Monoid m) => (a -> m) -> Maybe a -> m
  foldMap = error "todo"

class Traversable t where
  traverse
    :: (Applicative k)
    => (a -> k b) -> t a -> k (t b)

instance Traversable [] where
  traverse :: (Applicative k) => (a -> k b) -> [a] -> k [b]
  traverse = error "todo"

instance Traversable Maybe where
  traverse :: (Applicative k) => (a -> k b) -> Maybe a -> k (Maybe b)
  traverse = error "todo"

-- What can we do with Traversable?

-- | Use 'traverse' to "modify" elements, i.e. reclaim 'fmap'
fmapT :: Traversable t => (a -> b) -> t a -> t b
fmapT = error "todo"

-- | Use 'traverse' to set a new constant value
anonFmapT :: Traversable t => b -> t a -> t b
anonFmapT = error "todo"

-- | Use 'traverse' to monoidally fold values, i.e. reclaim 'foldMap'
foldMapT :: (Traversable t, Monoid b) => (a -> b) -> t a -> b
foldMapT = error "todo"

-- | Use 'traverse' to get the first element (if there is one)
-- **hint: use 'First'**
firstOfT :: (Traversable t) => t a -> Maybe a
firstOfT = error "todo"


-- Observations:
--
-- 'Traversable t => t X' is a data type that (possibly) contains
-- values of type 'X'.  We can retrieve the X(es), modify or replace
-- them, or monoidally fold them.
--
-- But there are many data types that may contain (or be able to
-- produce) a value of type 'X'.  Here is one: (X, Int).
-- Here is another one: [Either String (Maybe (NonEmpty X, String))]
--
-- These types are not Traversable (with respect to X, anyway).  Can
-- we generalise the behaviour of 'traverse' to arbitrary data types?

-- The type of 'traverse'; our starting point
type TTraversal a b =
  forall k t. (Applicative k, Traversable t)
  => (a -> k b) -> t a -> k (t b)

-- We want to replace the 't a' with an arbitrary type (that just
-- happens to maybe contain 'a' values, but the type need not
-- mention that).  We will call that type 's' and replace 't a'
-- with 's'.  To remove all trace of 'Traversable t' we must also
-- replace 't b'.  We cannot use 's' because in the "modify" scenario,
-- if we change type 'a' to 'b', then 's' can no longer be 's'.
-- So we call it 't' ('t' follows 's' as 'b' follows 'a').

type Traversal s t a b =
  forall k. (Applicative k)
  => (a -> k b) -> s -> k t

-- We can also define a shorthand for when 's' and 't' are
-- the same type, and 'a' and 'b' are the same type:

type Traversal' s a = Traversal s s a a

-- | Traversal for **both sides** of a pair
both :: Traversal (a, a) (b, b) a b
both = error "todo"

-- | A Traversal over any 'Traversable t => t a'
traversed :: (Traversable t) => Traversal (t a) (t b) a b
traversed = error "todo"

-- | Obtain a Traversal that can be composed with
-- another optic to "filter" it.
--
-- This is not a lawful Traversal in general.
-- You must take care not to invalidate the predicate!
--
filtered :: (a -> Bool) -> Traversal a a a a
filtered = error "todo"


-- If we instantiate the 'Applicative k' at 'Identity'
-- we obtain the behaviour of a setter.  We define a
-- type synonym for this specialisation:

type Setter s t a b = (a -> Identity b) -> s -> Identity t

-- | Modify the target(s) of a traversal
over :: Setter s t a b -> (a -> b) -> s -> t
over = error "todo"

-- | Set the target(s) of a traversal
set :: Setter s t a b -> b -> s -> t
set = error "todo"


{- EXERCISE: Fold -}

-- Recall: foldMapT f = getConst . traverse (Const . f)
--
foldMapOfT :: (Monoid r) => Traversal s t a b -> (a -> r) -> s -> r
foldMapOfT _ = error "todo"

-- Const's Applicative instance has a Monoid constraint; therefore
-- if we use a 'Traversal' we must also constrain 'Monoid r'.
-- We define a new type synonym 'Fold' which specialises the functor
-- at 'Const' and adds the 'Monoid' constraint:

type Fold' s t a b =
  forall r. (Monoid r)
  => (a -> Const r b) -> s -> Const r t

-- Due to Const, we can never actually do anything with 'b' or 't'.
-- Therefore we can fix 'b' at 'a', and 't' at 's', and halve the
-- number of type parameters.

type Fold s a = Fold' s s a a

-- | A Fold over any 'Foldable t => t a'
folded :: (Foldable t) => Fold (t a) a
folded = error "todo"

-- If we specialise foldMapOf's functor to 'Const r' we can
-- avoid the Monoid constraint here
foldMapOf :: ((a -> Const r b) -> s -> Const r t) -> (a -> r) -> s -> r
foldMapOf = error "todo"

-- We can extract this type to a type synonym:
type Getting r s a = (a -> Const r a) -> s -> Const r s

foldOf :: Getting a s a -> s -> a
foldOf = error "todo"

firstOf :: Getting (First a) s a -> s -> Maybe a
firstOf = error "todo"

{- Exercise: Lens -}

-- | Traverse over the first element of a pair
fstT :: Traversal (a, x) (b, x) a b
fstT = error "todo"

-- Observe that the Applicative constraint was not required
-- in the implementation of 'fstT'.  Functor is enough.

-- | Like fstT, but with 'Applicative' relaxed to 'Functor'
fstL
  :: forall k a b x. (Functor k)
  => (a -> k b) -> (a, x) -> k (b, x)
fstL = error "todo"

-- As we did with 'Traversal' and 'Fold', we define a type synonym
-- for this kind of function.  It is called 'Lens'.

type Lens s t a b =
  forall k. (Functor k)
  => (a -> k b) -> s -> k t

-- We can also define a shorthand for when 's' and 't' are
-- the same type, and 'a' and 'b' are the same type:

type Lens' s a = Lens s s a a

-- | Lens to the second element of a pair.
sndL :: Lens (x, a) (x, b) a b
sndL = error "todo"

-- Observe that we can use a Lens as a Traversal
setFst :: b -> (a, x) -> (b, x)
setFst = error "todo"

-- Observe that we can use a Lens as a Fold
fst :: (a, x) -> a
fst = error "todo"

-- Although we can use a Lens as a Fold to retrieve the target,
-- it is awkward to ask for the "fold of" a single-valued target.
-- We can define another function called 'view'.
view :: Getting a s a -> s -> a
view = error "todo"


{- EXERCISE: Composing optics -}

-- Observe: the optic types are type synonyms for
-- FUNCTIONS.  All functions take one argument, so
-- an optic function has the shape:
--
--   (a -> k b) -> (s -> k t)
--
-- for some Functor k.  The domain (a -> k b)
-- and (s -> k t) have a similar shape.  The
-- input function works on the "inner" type, and
-- the output function works on the "outer" type.
--
-- Hmm...

-- | Fold over the first elements in a Foldable of tuples.
foldFst :: (Foldable t) => Fold (t (a, x)) a
foldFst = error "todo"

{- Composing optics is just function composition! -}

{- Let us now solve a realistic query/update problem using optics -}

type StudentName = String
type CourseName = String
type StudentID = Integer

data Grade = G1 | G2 | G3 | G4 | G5 | G6 | G7
  deriving (Eq, Show)

type CourseEnrolment = (CourseName, Maybe Grade)

type AcademicRecord = [CourseEnrolment]

data Student = Student StudentName StudentID AcademicRecord
  deriving (Show)

studentName :: Lens' Student StudentName
studentName = error "todo"

studentID :: Lens' Student StudentID
studentID = error "todo"

studentAcademicRecord :: Lens' Student AcademicRecord
studentAcademicRecord = error "todo"

getGrade :: StudentID -> CourseName -> [Student] -> Maybe Grade
getGrade = error "todo"

-- Enrol a student in the course.  The grade is Nothing, because
-- the student has just enrolled.
--
-- The student is assumed to already be in the database.
--
enrol :: StudentID -> CourseName -> [Student] -> [Student]
enrol = error "todo"

-- Set the student's grade for the course.  If the student is not
-- enrolled in the course, do nothing.
setGrade :: StudentID -> CourseName -> Grade -> [Student] -> [Student]
setGrade = error "todo"


{- FINAL NOTES:

* There are more optics we are yet to explore.

* /lens/ is a comprehensive and stable optics library for Haskell:
  https://hackage.haskell.org/package/lens.  It uses the same
  representation of optics as we are exploring in this course.

* There are other representations of optics, with different
  tradeoffs.

* Other optic libraries for Haskell exist.  Some are compatible
  with /lens/, some are not.

* We used 'filtered' to obtain "Map"-like behaviour for lists.
  It is better to use proper Map types, and lawful lenses for
  getting at the values of particular keys.  The /lens/ library
  provides an abstraction for this pattern in "Control.Lens.At".

-}
