# COMP3400 - Prac #3

## Recap

Last prac you learned:

- Polymorphism
- The `Equal` (`Eq`) and `Order` type classes, and their laws
- The `CanMap` type class

This week's lecture covered:

- Types of types (*kinds*)
- `CanMap` is commonly called `Functor`
- Subclasses and superclasses
- Assignment 1 overview

By now you should be familiar with the following algebraic data
types (at least):

- `List a`
- `Optional a`
- `Parser a`
- `Vector6 a`
- `Or a b`
- `Pair a b`

Recall the following function types, and play the "spot the
difference" game:

```haskell
mapParser   :: (a -> b) -> Parser  a  -> Parser b
mapVector6  :: (a -> b) -> Vector6 a  -> Vector6 b
mapOptional :: (a -> b) -> Optional a -> Optional b
mapTree     :: (a -> b) -> Tree a     -> Tree b
mapList     :: (a -> b) -> List a     -> List b
```

Abstracting out the differences gives rise to the `Functor` type
class and its member function `fmap`:

```haskell
class Functor (f :: * -> *) where
  fmap :: (a -> b) -> f a -> f b
```

## `Functor` instances

**Exercise 1**: Have a go at exercise 1 in `Prac3.hs`.  You will
implement instances of `Functor` for several of the foundational
data types you have learned about.

## `Functor` derived operations

Abstraction lets us avoid repeating ourselves and enhances the
readability and maintainability of programs.

There is a trade-off between *number of instances* and *number of
derived operations*.  Many data types admit an instance of
`Functor`, but `Functor` has relatively few derived operations.
**Exercise 2**: implement some derived operations for `Functor`.

## Limitations of `Functor`

`fmap` maps a function of the shape `(a -> b)` (*single argument
function*) upon any `f a` where the `f` type constructor has an
instance of `Functor`.

What if we want to map an *n-argument function* onto multiple
Functor values?  For example:

```haskell
liftF2 :: (Functor f) => (a -> b -> c) -> f a -> f b -> f c
liftF2 = ???

addOptional :: Optional Int -> Optional Int -> Optional Int
addOptional optX optY = liftF2 (\x y -> x + y) optX optY
```

**Exercise 3**: Have a go at implementing `liftF2`.  What is the
result?


## Applicative functors

`liftF2` cannot be written.  `Functor` does not give us the power to
do it.  Using type holes, the point at which you get stuck reveals
the type of operation you need:

```haskell
_ :: f (a -> b) -> f a -> f b
```

`Functor` does not give us this operation.  The abstraction that
does is commonly called *applicative functor*, or just `Applicative`
in Haskell::

```haskell
class Functor f => Applicative (f :: * -> *) where
  (<*>) :: f (a -> b) -> f a -> f b
  pure :: a -> f a
```

`Applicative` is a subclass of `Functor`, so there are fewer
instances but more derived operations.  Also notice `pure`, which
lets us lift any "pure" value into an `Applicative` *context*.

We couldn't write `liftF2` with a `Functor` constraint, but
`Applicative` will let us write `liftA2`.  Go ahead and implement
`liftA2` (**Exercise 4**).


## `Applicative` instances

Many data types that have `Functor` instances can also have an
instance of `Applicative`.  Have a go at instancing `Applicative`
for some of the foundational data types (**Exercise 5**).

Which types do not admit an instance of `Applicative`?  Why?


## Building a parser

Consider the following (trivial and contrived) data format:

- A datum consists of two characters in sequence.

- The first character must be either 'd' or 'b'.

- If the first character is 'd', the second character must be a
  digit.

- If the first character is 'b', the next character must be either
  't' or 'f'.

Although this is a contrived example, similar problems often arise
in parsers, e.g. *run-length encoding*.  We will construct a
`Parser` for this format, with the name `parseFormat`.

**Exercise 6**: Implement some "building block" parsers: `satisfy`,
`satisfy`, `digit` and `tOrF`.  Then have a go at `parseFormat`.


## Limitations of `Applicative`

`parseFormat` cannot be implemented even with the power of
`Applicative`.  We need a more powerful abstraction that allows us
to decide how to continue a computation *based on an intermediate
result*.  That is, we need something of the shape:

```haskell
class NewAbstraction f where
    whatNext :: f a -> (a -> f b) -> f b
```

The common name for this abstraction is `Monad`:

```haskell
class Applicative m => Monad (m :: * -> *) where
  (>>=) :: m a -> (a -> m b) -> m b
```

**Exercise 7**: Implement the `Monad` instance for `Parser`, then go
back and complete the implementation of `parseFormat`.

Also implement instances of `Monad` for `Optional`, `List`, `(Or
a)`.  What other types could have an instance?  Why or why not?
