# COMP3400 - Prac #2

Last prac you learned:

* Values
* Functions
* Types
* Operators
* Data types
* Pattern matching

That's about 80% of Haskell. This prac we'll go through most of the rest.

## 1. Polymorphism

Let's look at the identity function for `Int`:

```haskell
idInt :: Int -> Int
idInt x = x
```

Now let's look at the same function for `String`:

```haskell
idString :: String -> String
idString x = x
```

The type has changed but it's the exact same code!

In the last prac we said that types start with uppercase - a bit of a lie.
Specific types start with uppercase but if we use a lowercase name, we create
a _polymorphic_ type.

```haskell
identity :: a -> a
identity x = x
```

And this function can be used with any data type:

```
Prelude> identity 1
1
Prelude> identity 'a'
'a'
Prelude> identity True
True
```

Polymorphism also applies to data types. Here the lowercase `a` is a type
variable.

```haskell
data Optional a
  = Empty
  | Full a
```

`Optional` can not be used as a type, it is a _type constructor_. It makes
sense to talk about the types `Optional Int`, `Optional Bool`, `Optional
Person` but not `Optional` by itself.

Take a guess what the types of the following values are. Use `:type` to check
your answers.

```haskell
Full 'a'
Full False
Full not
Empty
```

## 2. Type-classes

Load up the `prac2.hs` file using `ghci`:

```
Prelude> :load prac2.hs
[1 of 1] Compiling Main             ( prac2.hs, interpreted )
Ok, one module loaded.
```

This file has a bunch of uses of `error`. This is a special bit of code which
throws an exception when used. Replace the `error` implementations with
working code.

### 2.1 Equal

We might want to compare two things. Equality can be a tricky subject both in
mathematics and in programming but we can talk about a very simple version
using this type signature:

```haskell
a -> a -> Bool
```

Which can be read as "we can answer yes or no when given two values."

But what type of values? We might want to compare strings, ints and booleans
today. Tomorrow we might decide that we want to compare optionals of these!

A type-class is often a good idea when we start thinking about using the same
operations on many different types. The `prac.hs` file defines the following
`Equal` class:

```haskell
class Equal a where
  isEqual :: a -> a -> Bool
```

Below this definition are a few instances which you should try to implement.

This version of equality usually has the following _algebraic laws_ which
must be satisfied for code to work properly:

#### 2.1.1 Reflexivity

```haskell
forall a.
  isEqual a a = True
```

**Exercise:** Think of a broken implementation for `Bool` where this is not true.

#### 2.1.2 Symmetry

```haskell
forall a b.
  isEqual a b = isEqual b a
```

**Exercise:** Think of a broken implementation for `Bool` where this is not true.

#### 2.1.3 Transitivity

```haskell
forall a b c.
  (isEqual a b && isEqual b c) = True
implies
  isEqual a c = True
```

**Exercise:** Think of a broken implementation for `Bool` where this is not true.

### 2.2 Order

The `Order` class _extends_ the `Equal` class. It provides an additional
method called `compared`. All types with instances of `Order` _must_ also
have instances for `Equal`.

Does the `comparedEqual` function look familiar?

The `Order` class' `compared` method is powerful enough to _derive_ an
implementation for the `isEqual` method. Since a type has to implement
`Equal` when implementing `Order`, we have to ensure the `compared` method is
_consistent_ with the `isEqual` method.

That is, we need to ensure the following law:

```haskell
forall a b.
  isEqual (compared a b) EqualTo = isEqual a b
```

The `Order` class has some additional laws for its method:

#### 2.1.1 Reflexivity

```haskell
forall a.
  compared a a = EqualTo
```

#### 2.1.2 Antisymmetry

```haskell
forall a b.
  (lessThanOrEqual a b && lessThanOrEqualTo b a) = True
implies
  compared a b = EqualTo
```

#### 2.1.3 Transitivity

```haskell
forall a b c.
  (lessThanOrEqual a b && lessThanOrEqualTo b c) = True
implies
  lessThanOrEqualTo a c = True
```

### 2.3 Exercises from lecture

The lecture left you with some exercises. Have a go at implementing them.

Test your code against these cases:

```
*Main> mapList not (Cons True (Cons False Nil))
Cons False (Cons True Nil)

*Main> mapOptional (\x -> x + 1) (Full 1)
Full 2

*Main> runParser (mapParser pred (charParser 'x')) "xyz"
ParseSuccess 'w' "yz"

*Main> mapVector6 succ (Vector6 'a' 'b' 'c' 'd' 'e' 'f')
Vector6 'b' 'c' 'd' 'e' 'f' 'g'
```

### 2.4 CanMap

We've seen a lot of types that can be mapped. Remember:

> A type-class is often a good idea when we start thinking about using the
> same operations on many different types

The `CanMap` class does this for mapping.

This type-class is actually very well known and it is in Haskell's standard
library. Instead of `CanMap`, we call it `Functor`. It has the following
laws:

#### 2.4.1 Identity

```haskell
  mapAnything (\x -> x) = (\x -> x)
```

**Exercise:** Think of a broken implementation for `List` where this is not true.

#### 2.4.1 Composition

```haskell
forall f g a.
  mapAnything (\x -> f (g x)) a = mapAnything f (mapAnything g a)
```
