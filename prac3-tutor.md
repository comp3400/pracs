# COMP3400 - Prac #3 tutor notes

## Recap

Subclass/superclass

- Superclass has more instances; subclass has more derived
  operations
- `Eq` is superclass of `Ord`
- `Functor` has kind *type to type* (`* -> *`)

What is the difference between `Or a b` and `Pair a b`

## Exercise 1: Implementing `Functor`

Student have already done this for several data types, but as
*specialised* functions e.g. `mapList`, `mapOptional`, etc.

Thought provoking questions:

- Why is it `instance Functor List`, but
  `instance Functor (Or a)`? 

- Why can't you implement `instance Functor Or`?

- Why can't you implement `instance Functor Int`?

- What is interesting about the implementations for
  `NonEmptyList` and `Tree`?

- What if we wanted to map over the 'a' in `Pair a b`
  or `Or a b`?


## Exercise 2: `Functor` derived operations

If an abstraction has few instances and few derived operations, it
is probably not a good abstraction.


## Exercise 3: `liftF2`

Work the example using type holes.  The point at which we get stuck
has a hole of type `f (b -> c) -> f c`.


## Applicative 

Compare the types of `fmap` and `(<*>)`.


## Exercise 3: `liftA2`

Worked example, after a good number of students have reached this
point.

## Exercise 5: `Applicative` instances

Thought-provoking questions:

- How many possible implementations of the `Vector6` instances?

- Which instance makes the most sense?  Why?  Does this instance
  even make sense?

- What is the problem with `instance Applicative (Pair a)`


## Monad

- `return` = `pure`.  In the beginning `Applicative` and `Monad`
  were both direct subclasses of `Functor` but the class heirarchy
  was later fixed.  `return` is vestigal.

- `instance Monad Vector6`.  You can make it type-check.  What is
  the problem?

- `IO` has a `Monad` instance (therefore `Applicative` and `Functor`
  also).

## Questions about assignment.
