with import <nixpkgs> { };

runCommand "comp3400-pracs" {
  buildInputs = [
    (texlive.combine { inherit (texlive) scheme-medium; })
    pandoc
  ];
  src = lib.sourceFilesBySuffices ./. [".md"];
} ''
  mkdir -p $out
  for f in $src/*.md; do
    name="$(basename $f .md)"
    pandoc --variable urlcolor=blue $f -o $out/$name.pdf
  done
''
