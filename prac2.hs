-- 2.1 Equal

class Equal a where
  isEqual :: a -> a -> Bool

instance Equal Bool where
  isEqual =
    error "TODO: Bool#isEqual"

data Optional a
  = Empty
  | Full a
  deriving Show

instance Equal a => Equal (Optional a) where
  isEqual =
    error "TODO: Optional#isEqual"

data List a
  = Nil
  | Cons a (List a)
  deriving Show

instance Equal a => Equal (List a) where
  isEqual =
    error "TODO: List#isEqual"

lookupByKey :: Equal key => key -> List (key, value) -> Optional value
lookupByKey =
  error "TODO: lookupByKey"

--- 2.2 Order

data Comparison = LessThan | EqualTo | GreaterThan

instance Equal Comparison where
  isEqual =
    error "TODO: Comparison#isEqual"

class Equal x => Order x where
  compared :: x -> x -> Comparison

instance Order Bool where
  compared =
    error "TODO: Bool#compared"

instance Order a => Order (List a) where
  compared =
    error "TODO: List#compared"

comparedEqual :: Order x => x -> x -> Bool
comparedEqual =
  error "TODO: comparedEqual"

lessThanOrEqual :: Order x => x -> x -> Bool
lessThanOrEqual =
  error "TODO: lessThanOrEqual"

isSorted :: Order x => List x -> Bool
isSorted =
  error "TODO: isSorted"

-- 2.3 Exercises from the lecture

mapList :: (a -> b) -> List a -> List b
mapList =
  error "TODO: mapList"

flipList ::
  List (a -> b) -> a -> List b
flipList list a =
  mapList (\func -> func a) list

mapOptional :: (a -> b) -> Optional a -> Optional b
mapOptional =
  error "TODO: mapOptional"

flipOptional ::
  Optional (a -> b) -> a -> Optional b
flipOptional opt a =
  mapOptional (\func -> func a) opt

data ParseResult x
  = ParseError String
  | ParseSuccess x String
  deriving Show

data Parser x
  = Parser (String -> ParseResult x)

runParser :: Parser a -> String -> ParseResult a
runParser (Parser f) =
  f

charParser :: Char -> Parser Char
charParser c =
  Parser (\s ->
      case s of
        (x:xs) ->
          if x == c
          then ParseSuccess x xs
          else ParseError (show x ++ " is not " ++ show c)
        [] ->
          ParseError "unexpected end of input"
    )

mapParser :: (a -> b) -> Parser a -> Parser b
mapParser =
  error "TODO: mapParser"

flipParser ::
  Parser (a -> b) -> a -> Parser b
flipParser prsr a =
  mapParser (\func -> func a) prsr

data Vector6 a
  = Vector6 a a a a a a

mapVector6 :: (a -> b) -> Vector6 a -> Vector6 b
mapVector6 =
  error "TODO: mapVector6"

flipVector6 ::
  Vector6 (a -> b) -> a -> Vector6 b
flipVector6 v6 a =
  mapVector6 (\func -> func a) v6

--- 2.4 CanMap

class CanMap f where
  mapAnything :: (a -> b) -> f a -> f b

instance CanMap List where
  mapAnything = mapList

instance CanMap Optional where
  mapAnything = mapOptional

instance CanMap Parser where
  mapAnything = mapParser

instance CanMap Vector6 where
  mapAnything = mapVector6

flipAnything ::
  Functor f =>
  f (a -> b) ->
  a ->
  f b
flipAnything fa a =
  error "TODO: flipAnything"
