# COMP3400 - Prac #1

Goals for this prac session are:

1. What is a function?
2. How do I use Haskell?

Reach out to Tony or Brian, or message [#comp3400 on Freenode
IRC](http://webchat.freenode.net/?randomnick=1&channels=%23comp3400&uio=d4)
if you have trouble or questions!

## 1. Functions

### 1.1

Is the following a _function_? Why or why not?

```
f(true) = false
f(false) = true
```

### 1.2

Is the following a _function_? Why or why not?

```
f(true) = false
```

### 1.3

Is the following a _function_? Why or why not?

```java
int plusOne(int x) {
  return x + 1;
}
```

### 1.4

Is the following a _function_? Why or why not?

```java
String greeting(String name) {
  Date now = new Date();
  return "Hello " + name + ", today is " + now.toString();
}
```

### 1.5

Is the following a _function_? Why or why not?

```java
int logPlus(int x, int y) {
  logger.debug("Adding " + x + " " + y);
  return x + y;
}
```

## 2. Haskell

We use the Glasgow Haskell Compiler (GHC) to compile and evaluate Haskell
programs.

## 2.1 Getting GHC

GHC 8.6.x is the recommended version but you should have no problem
completing this unit as long as you use any 8.x version.

The following instructions are just the easiest methods we've found, feel
free to install GHC using your favourite package manager, downloading
binaries directly or even building from source.

If you're familiar with more complicated Haskell build tools (e.g. Stack) you
may use them but we are only comfortable providing support for GHC.

### 2.1.1 Windows

The [Chocolatey](https://chocolatey.org/) package manager is an easy way to
get GHC on Windows. Install Chocolatey then run:

```
$ choco install msys2
$ choco install ghc --version=8.6.5
```

### 2.1.2 macOS & Linux

[ghcup](https://www.haskell.org/ghcup/) is an installer for macOS and Linux.
You can get the recommended version by running the following script:

```
$ curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | sh
```

The instructions will tell you to "source" a file, you can do this by
appending the following line to your `$HOME/.bashrc` file:

```sh
source "$HOME/.ghcup/env"
```

Then either restart your terminal or run that line manually for the first
time.

## 2.2 GHCi

GHC includes a command called _ghci_, which is an interactive tool for
developing Haskell programs.

We're going to need a directory to put some files in:

```
$ mkdir comp3400
$ cd comp3400
```

To start ghci, run it from your terminal:

```
$ ghci
GHCi, version 8.4.3: http://www.haskell.org/ghc/  :? for help
Prelude>
```

The prompt is telling you that it has included the "Prelude" library by
default. This library includes a bunch of functions. Let's take a look at the
`not` function.

Enter `not True` into the prompt and GHCi will evaluate the Haskell code and
give you the result.

```
Prelude> not True
False
```

You can ask GHCi for the type with the `:type` command:

```
Prelude> :type not
not :: Bool -> Bool
```

Open up a new file using a text editor and save the following contents as
`Prac1.hs` in the directory we created above:

```
x = False
```

We can load this into GHCi by using the `:load` command with the filename:

```
Prelude> :load Prac1.hs
[1 of 1] Compiling Main             ( Prac1.hs, interpreted )
Ok, one module loaded.
*Main>
```

We can use the `x` binding that we created:

```
*Main> x
False
*Main> not x
True
```

Now add the following line to the `Prac1.hs` file and save it:

```
y = True
```

Instead of typing in `:load Prac1.hs` each time we make a change, we can tell
GHCi to `:reload`, without the filename:

```
*Main> :reload
[1 of 1] Compiling Main             ( Prac1.hs, interpreted )
Ok, one module loaded.
```

And now we have access to the `y` binding:

```
*Main> x || y
True
```

When you're ready, leave the ghci tool with the `:quit` command:

```
*Main> :quit
Leaving GHCi.
```

## 2.3 Syntax

### 2.3.1 Values

In Haskell we write _bindings_ to values to give them names. Other languages
and tools call these _variables_ but in Haskell the equal sign actually means
_equal_ so these values don't _vary_ in the same way. Sometimes we're loose
with terminology though, that's alright!

We have already made two bindings in our `Prac1.hs` file:

```haskell
x = False
y = True
```

These are bindings to two boolean values. Here's a bunch of examples of other
values we can play with, each preceded by a comment:

```haskell
-- Here is an Int
v1 = 1

-- Here is a Double
v2 = 2.3

-- Here is a Char
v3 = 'a'

-- Here is a String
v4 = "Hello world"

-- Here is a list of Int
v5 = [4, 6, 8]

-- Here is a pair (tuple) of Char and Int
v6 = ('a', 3)

-- Here is a function applied with a Bool
v7 = not True
```

### 2.3.2 Type signatures

Haskell has _type-inference_ so that we don't have to manually tell it what
type everything is. It's usually good for documentation and readability to
give our top-level bindings an explicit type, and only let Haskell infer
types in nested code.

We use double colon to say "has the type" - here's the above but with
explicit types:

```haskell
v1 :: Int
v1 = 1

v2 :: Double
v2 = 2.3

v3 :: Char
v3 = 'a'

v4 :: String
v4 = "Hello world"

v5 :: [Int]
v5 = [4, 6, 8]

v6 :: (Char, Int)
v6 = ('a', 3)

v7 :: Bool
v7 = not True
```

### 2.3.3 Functions

One form of values that we haven't covered yet are _functions_. To create a
function, we use a _lambda_.

```haskell
f = \x -> x + 1
```

Binding to a lambda is so common that Haskell has some _syntactic sugar_.
It's a different syntax but this means **exactly the same thing as above**.

```haskell
f x = x + 1
```

The type of a function uses an arrow:

```haskell
f :: Int -> Int
f x = x + 1
```

A very important thing in Haskell is that _every_ function only takes a
_single_ parameter. If we wanted to write a function which takes two
parameters, we actually write a function which returns a function. Sometimes
we're a bit relaxed with terminology and _say_ this takes **two** parameters,
but we're technically wrong!

```haskell
g = \x -> (\y -> even (x + y))
```

Lambdas in Haskell are _right associative_ which means parentheses go to the
right if they're left off:

```haskell
g = \x -> \y -> even (x + y)
```

A lambda returning a lambda is so common in Haskell, there's some more syntactic sugar.

```haskell
g = \x y -> even (x + y)
```

This combines in with the syntactic sugar above so we can also write:

```haskell
g x y = even (x + y)
```

This is a function returning a function, so the type is:

```haskell
g :: Int -> (Int -> Bool)
g x y = even (x + y)
```

The type has the same right associative property as the lambda, so can be
written as:

```haskell
g :: Int -> Int -> Bool
g x y = even (x + y)
```

We can apply arguments to a function by separating the function and an
argument with a space:

```haskell
v10 :: Int
v10 = (\x -> x + 1) 10

v11 :: Int
v11 = f 10
```

Function application has _left assocative_ - opposite of lambdas and function
types!

```haskell
v12 :: Bool
v12 = (g 10) 11

v13 :: Bool
v13 = g 10 11
```

### 2.3.4 Operators

We used addition using the `+` operator. Operators are just functions which
have names made of symbols. By default, operators are in _infix_ notation. We
can put them into _prefix_ notation by surrounding the operator with
parentheses.

```haskell
v14 :: Int
v14 = (+) 1 2
```

Non-operator function names are also in prefix notation by default, we can
change them into infix by surrounding them with backticks.

```haskell
v15 :: Bool
v15 = 1 `g` 2
```

Because operators are **just** functions, we can define our own. The type of an
operator must be in prefix but the definition can be either prefix or infix.

```haskell
(+!) :: Int -> Int -> Int
(+!) x y = x + y + 1

v16 :: Int
v16 = 1 +! 2

(!+) :: Int -> Int -> Int
x !+ y = x + y + 2

v17 :: Int
v17 = 1 !+ 2
```

### 2.3.5 Data types

We can write our own data types in Haskell by specifying how to construct the
data. A data type can have zero or more constructors and each constructor can
have zero or more arguments.

We can use the `:info` command to look at how the `Bool` type is defined:

```
Prelude> :info Bool
data Bool = False | True        -- Defined in ‘GHC.Types’
```

The data type is called `Bool` and it has two constructors: `True` and `False`.

Haskell has some rules for names. Type names start with upper case. Bindings
start with lower case or are made of symbols. Constructors also start with
upper case but live in a different namespace to types; We can use the same
name for a constructor and a type - Haskell's rules of when we can use types
and constructors mean this is never ambiguous.

```haskell
data Person = Person String Int
```

This defines a `Person` data type with one constructor of the same name. The
constructor takes two arguments: a `String` and an `Int`.

Constructors are functions but offer something special that we'll see under
the pattern matching section below. We can use the constructor as a function
to construct a value of its type:

```haskell
brian :: Person
brian = Person "Brian" 29
```

Remember the double colon means "has type of" so Haskell knows we're
refering to the type on that line. On the next line, Haskell sees we're
using `Person` as a value, so we must be talking about the constructor.

### 2.3.6 Pattern matching

To work with data types we can break them down. Pattern matching lets us
handle each constructor of a data type and get access to the constructor's
fields.

We can use "case ... of" to handle each case of `Bool`:

```haskell
boolToInt :: Bool -> Int
boolToInt b =
  case b of
    True -> 1
    False -> 0
```

Haskell is whitespace sensitive so the `case` needs to be indented more than
the function name and the constructors have to be indented more than the
`case` keyword.

If the constructor has arguments we can extract them using the case:

```haskell
personGreeting :: Person -> String
personGreeting p =
  case p of
    Person name age -> "Hello " ++ p
```

`name` and `age` are bindings to the `Person` constructor's arguments.

Pattern matching on arguments to functions is extremely common. Haskell
provides some sugar for this too. We can rewrite the above:

```haskell
boolToInt :: Bool -> Int
boolToInt True = 1
boolToInt False = 0

personGreeting :: Person -> String
personGreeting (Person name age) = "Hello " ++ p
```

Instead of giving a name to the function argument, we replace the argument
with the cases.
