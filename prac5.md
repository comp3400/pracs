# COMP3400 - Prac #5

Last prac covered:

- Type isomorphism worked examples
- Applicative and Monad derived operations
- Parser combinators; building a non-trivial parser
- IO operations

This prac covers:

- Review based on Assignment 1:
    - Guards and pattern matching
    - Avoid non-total functions
    - Avoid boolean blindness
    - Avoid sentinel values

- Automated testing (`Test.Framework` / `QuickCheck`)

- More data types: `Identity`, `Tagged`, `Const`, `Compose`

- Monoids


## Review

### Guards

The consider the following function, implemented using a nested
if-then-else expression:

```haskell
clampWhen :: (Int -> Bool) -> Int -> Int -> Int -> Int
clampWhen pred lo hi x =
  if not (pred x)
  then x
  else
    if x < lo
    then lo
    else
      if x > hi
      then hi
      else x
```

The following definition is equivalent, but uses *guard* syntax.

```haskell
clampWhen :: (Int -> Bool) -> Int -> Int -> Int -> Int
clampWhen pred lo hi x =
  | not (pred x) = x
  | x < lo = lo
  | x > hi = hi
  | otherwise = x
```

Guards are usually more readable, therefore preferred, where an
implementation would otherwise require a nested if-then-else
expression.  Where only a single if-then-else is required, use
whichever syntax you prefer.

Note that `otherwise` is not syntax, but a synonym for `True`:

```haskell
otherwise :: Bool
otherwise = True
```

Guards have an additional nice feature: they can be combined with
pattern matches.  If none of the guards for a pattern evaluates
`True`, the next pattern will be tried.  For example:

```haskell
-- | Get the Full value if it satisfies the predicate,
--   otherwise return the default value
fullIf :: (a -> Bool) -> a -> Optional a -> Optional a
fullIf pred _   (Full a) | pred a = a
fullIf _    def _                 = def
```


### Pattern matching and `Optional`/`Maybe`

The following is an antipattern:

```haskell
hasTwoInARow :: (Eq a) => List a -> Bool
hasTwoInARow (a `Cons` b `Cons` t)
  | a == b = True
  | otherwise = hasTwoInARow (b `Cons` t)
hasTwoInARow _ = False

-- | Precondition: hasTwoInARow l == True
getTwoInARow :: (Eq a) => List a -> a
getTwoInARow (a `Cons` b `Cons` t)
  | a == b = a
  | otherwise = getTwoInARow (b `Cons` t)
getTwoInARow _ = error "ran out of list"

listProgram :: (Eq a, Show a) => List a -> IO ()
listProgram l
  | hasTwoInARow l =
      putStrLn ("Two in a row: " ++ show (getTwoInARow l))
  | otherwise = putStrLn "There aren't two in a row"
```

It is an antipattern for several reasons:

1. The same pattern of recursive destructuring of the input
   structure is repeated in two functions.

2. `getTwoInARow` is non-total, i.e. it does not return a result for
   some inputs (it crashes the program instead), i.e. it does not
   fulfil its type.  (Haskell does not check or enforce totality,
   but some other languages can.)

3. Unlike pattern matches, GHC cannot check that guards
   *exhasutively* consider the possible return values.

A better approach is to look for a match, and return the match value
*if there is one*, otherwise return a value indicating the absense
of a result.  The `Optional` or `Maybe` type is suitable:

```haskell
findTwoInARow :: (Eq a) => List a -> Optional a
findTwoInARow = error "todo; this is one of the prac exercise"
```


### Non-total functions

All function takes one value and returns one value.  A computation
that does return a value for every possible input value is not a
function.  We call such functions *non-total* (you may also see the
term *partial function*).

The Haskell programming language does allow defining non-total
functions.  There are several ways a function could be non-total:

- Infinite recursion
- *Non-exhaustive patterns*
- *Irrefutable patterns*
- The `error` facility
- Exceptions

Non-total functions should be avoided.  Unfortunately, even the
*base* library exports many non-total functions including:

```haskell
head :: [a] -> a
tail :: [a] -> [a]
read :: (Read a) => String -> a
readLn :: (Read a) => IO a
(!!) :: [a] -> Int -> a
error :: String -> a
undefined :: a
minimum, maximum :: (Ord a) => [a] -> a
```

Be very reluctant to use these functions, or other non-total
functions, in your programs.  There is almost always a better way.
*Remember, if you need help, ask.*

In some cases, the use of a non-total function is unavoidable or
otherwise warranted.  Some examples include:

- Routines from `System.Exit` for intentionally terminating the
  program
- `div`, `mod`, `(/)` and some other functions can throw *divide by
  zero* exceptions; their use should have appropriate guards

A final kind non-termination is infinite data.  Haskell is a
non-strict language, allowing infinite recursive structures to be
defined.  If you try to evaluate (the whole of) an infinite
structure, the program will not terminate.  For example:

```
> Prelude.length [0..]  -- CPU warms, free memory diminishes
^CInterrupted.
```


### Boolean blindness

Consider a person type:

```haskell
type Name = String

data Date = Date Year Month Day

instance Eq Date where
  Date y m d == Date y' m' d' =
    y == y' && m == m' && d == d'

instance Ord Date where
  Date y m d <= Date y' m' d'
    | y < y'              = True
    | y == y' && m < m'   = True
    | y == y' && m == m'  = d <= d'
    | otherwise           = False

data Person = Person Name Date {-of birth-} Sex
```

How should we define the `Sex` type?  In our view of the world there
are two values.  `Boolean` is a type with two inhabitants.

```haskell
type Sex = Boolean

-- Is Bobby male or female?
bobby :: Person
bobby = Person "Bobby" bobbyDoB True

-- to `b` or to `not b`; that is the question
isFemale :: Person -> Bool
isFemale (Person _ _ b) = _
```

This phenomenon is called *boolean blindness*.  Boolean blindness
leads to bugs because when deprived of context one forgets, becomes
confused or cannot deduce what `False` and `True` actually mean.

A better definition for `Sex` is:

```haskell
data Sex = Female | Male

isFemale :: Person -> Bool
isFemale (Person _ _ Female) = True
isFemale _ = False
```

*Never hesitate* to define a new data type that clearly communicates
(to yourself, other humans, and the compiler) the meaning of data
and the shape of the problem you are trying to solve.


### Sentinel values

A new requirement emerges: we must implement a data entry program
for `Person`.  It is optional to input the data of birth.  How
shall we modify our types to accommodate this use case?  Here is one
idea:

```haskell
data Sex = Female | Male | SexNotSpecified
```

`SexNotSpecified` is a kind of *sentinel value*; a value that
inhabits the type but is not really part of the domain of that type.
Is this a good change?  Depending on use case you might be able to
justify this change.  But what does the `Sex` data type *mean*?  Is
"not specified" one of the sexes, semantically speaking?

How about adding a sentinel value to `Date`?

```haskell
data Date
  = Date Year Month Day
  | DateNotSpecified

instance Eq Date where
  Date y m d == Date y' m' d' = y == y' && m == m' && d == d'
  DateNotSpecified == DateNotSpecified = True
  _ == _ = False

instance Ord Date where
  Date y m d <= Date y' m' d'
    = (y,m,d) <= (y',m',d') -- equiv. to prev. definition
  _ <= _ =
    _huh -- what does it mean to compare with DateNotSpecified?
```

Adding a sentinel constructor to `Date` raises awkward questions.
Semantically, "not specified" is not a date.  How do you compare a
date to the absense of a date?  If a design (change) poses questions
like these, it is a strong indication that the design (change) is
not appropriate.

A better approach would be to use `Maybe` or `Optional`:

```haskell
data Sex = Female | Male
data Date = Date Year Month Day
data Person = Person Name (Maybe Date) (Maybe Sex)
```

`Maybe Sex` has three *inhabitants*, like the three-constructor
variant of `Sex` from earlier.  And `Maybe Date` has the same number
of inhabitants as the modified `Date`.  But the structure
communicates a different meaning and averts the problematic
intrusion of sentinel values into the underlying data types.


## Exercises

This week's prac exercise module imports `Test.Framework`.  It can
be downloaded from
https://gitlab.com/comp3400/test-framework/-/blob/master/Test/Framework.hs.

Make `Test.Framework` available at `Test/Framework.hs` (relative to
`Prac5.hs`, then open `Prac5.hs` for editing and get started.  
