{-# LANGUAGE InstanceSigs #-}

{-# OPTIONS_GHC -Wall -Wno-unused-imports #-}

module Prac5 where

import Prelude (error, (-), (++), (.))
import Control.Applicative (Applicative(..))
import Control.Monad (Monad(..))
import Data.Bool
import Data.Eq
import Data.Functor
import Data.Int
import Data.Ord
import System.IO (IO, putStrLn)
import Text.Show

import Test.Framework

{- PRELUDE -}


data List a = Nil | Cons a (List a)
  deriving (Eq, Show)
infixr 5 `Cons`

instance Functor List where
  fmap _ Nil = Nil
  fmap f (x `Cons` xs) = f x `Cons` fmap f xs

foldRight :: (a -> b -> b) -> b -> List a -> b
foldRight _ b Nil      = b
foldRight f b (h `Cons` t) = f h (foldRight f b t)


data Optional a = Empty | Full a
  deriving (Eq, Show)


data NonEmptyList a = NonEmptyList a (List a)
  deriving (Eq, Show)


{- TOTAL AND NON-TOTAL FUNCTIONS -}

-- | Get the head of the list
--
-- What is wrong with this type?
--
listHead :: List a -> a
listHead = error "todo"

replicateA :: (Applicative k) => Int -> k a -> k (List a)
replicateA = error "todo"

instance Arbitrary a => Arbitrary (List a) where
  arbitrary = error "todo"

prop_listHeadAnonFmap :: List a -> Int -> Bool
prop_listHeadAnonFmap xs n = listHead (n <$ xs) == n

-- | This is a better type for 'head'.
head :: List a -> Optional a
head = error "todo"

-- | A NonEmptyList always has a head, so we don't
-- need 'Optional'.
nehead :: NonEmptyList a -> a
nehead = error "todo"


hasTwoInARow :: (Eq a) => List a -> Bool
hasTwoInARow (a `Cons` b `Cons` t)
  | a == b = True
  | otherwise = hasTwoInARow (b `Cons` t)
hasTwoInARow _ = False

-- | Precondition: hasTwoInARow l == True
getTwoInARow :: (Eq a) => List a -> a
getTwoInARow (a `Cons` b `Cons` t)
  | a == b = a
  | otherwise = getTwoInARow (b `Cons` t)
getTwoInARow _ = error "ran out of list"

listProgram :: (Eq a, Show a) => List a -> IO ()
listProgram l
  | hasTwoInARow l = putStrLn ("Two in a row: " ++ show (getTwoInARow l))
  | otherwise = putStrLn "There aren't two in a row"

findTwoInARow :: (Eq a) => List a -> Optional a
findTwoInARow = error "todo"

betterListProgram :: (Eq a, Show a) => List a -> IO ()
betterListProgram = error "todo"


{- MORE DATA TYPES: Identity -}

data Identity a = Identity { getIdentity :: a }

instance Functor Identity where
  fmap :: (a -> b) -> Identity a -> Identity b
  fmap = error "todo"

instance Applicative Identity where
  pure :: a -> Identity a
  pure = error "todo"

  (<*>) :: Identity (a -> b) -> Identity a -> Identity b
  (<*>) = error "todo"

instance Monad Identity where
  (>>=) :: Identity a -> (a -> Identity b) -> Identity b
  (>>=) = error "todo"


{- MORE DATA TYPES: Tagged -}

data Tagged a b = Tagged { getTagged :: b }

instance Functor (Tagged z) where
  fmap :: (a -> b) -> Tagged z a -> Tagged z b
  fmap = error "todo"

instance Applicative (Tagged z) where
  pure :: a -> Tagged z a
  pure = error "todo"

  (<*>) :: Tagged z (a -> b) -> Tagged z a -> Tagged z b
  (<*>) = error "todo"

-- Exercise: can you instance Monad for 'Tagged z'?


{- MORE DATA TYPES: Const -}

data Const c a = Const { getConst :: c }

instance Functor (Const c) where
  fmap :: (a -> b) -> Const c a -> Const c b
  fmap = error "todo"

-- Exercise: can we implement Applicative?
-- If not, why not?
-- What abstraction(s) would we need to do it?
-- What laws should that abstraction obey?
--
instance Applicative (Const c) where
  pure :: a -> Const c a
  pure = error "todo"

  (<*>) :: Const c (a -> b) -> Const c a -> Const c b
  (<*>) = error "todo"


{- MORE DATA TYPES: Compose -}

data Compose f g a = Compose { getCompose :: f (g a) }

instance (Functor f, Functor g) => Functor (Compose f g) where
  fmap :: (a -> b) -> Compose f g a -> Compose f g b
  fmap = error "todo"

instance (Applicative f, Applicative g) => Applicative (Compose f g) where
  pure :: a -> Compose f g a
  pure = error "todo"

  (<*>) :: Compose f g (a -> b) -> Compose f g a -> Compose f g b
  (<*>) = error "todo"

-- Exercise: try and write a Monad instance.
-- If you cannot, why not?
instance (Monad f, Monad g) => Monad (Compose f g) where
  (>>=) :: Compose f g a -> (a -> Compose f g b) -> Compose f g b
  (>>=) = error "todo"


{- MONOID -}

class Monoid' a where
  mempty' :: a
  mappend' :: a -> a -> a

class Semigroup a where
  (<>) :: a -> a -> a

class (Semigroup a) => Monoid a where
  mempty :: a

mappend :: (Semigroup a) => a -> a -> a
mappend = (<>)

instance Semigroup (List a) where
  (<>) :: List a -> List a -> List a
  (<>) = error "todo"

instance Monoid (List a) where
  mempty :: List a
  mempty = error "todo"

-- LAWS!

-- associativitiy
prop_semigroup_assoc_List
  :: List Int -> List Int -> List Int -> Bool
prop_semigroup_assoc_List =
  error "todo"

-- left identity
prop_monoid_id_left_List :: List Int -> Bool
prop_monoid_id_left_List = error "todo"

-- right identity
prop_monoid_id_right_List :: List Int -> Bool
prop_monoid_id_right_List = error "todo"

{- Thought exercises:

* Monoids and semigroups are everywhere.  What types can you
  think of that have an associative combining function?  What
  types additionally have an identity element?

* What types /in this module/ could have lawful instances?
  You could have a go at implementing them, and writing tests.

* What types are Semigroup but not Monoid?

* Monoid is a subclass of Semigroup.  Can you think of other
  operations (with associated laws) that could further extend
  this abstraction heirarchy?

-}


-- Now we can write Applicative for 'Const c'
{-
instance (Monoid c) => Applicative (Const c) where
  pure :: a -> Const c a
  pure = error "todo"

  (<*>) :: Const c (a -> b) -> Const c a -> Const c b
  (<*>) = error "todo"
-}
