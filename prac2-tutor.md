# COMP3400 - Prac #2 tutor notes

## 1. Functions

The goal of this section is to discuss why something may or may not be a
function. Going through each one with the whole class should create some
interesting discussion.

### 1.1

This is a function, it only maps every input of the Boolean set to a value.

### 1.2

Assuming we're working with the set of Boolean values `{true, false}` then
this is _not_ a function since `f` is not defined for input `false`.

We didn't write down the type of this function so you _could_ say this
function has the input set of only `{true}` and that would make it a function
- but most programming languages don't allow you to restrict sets in this
way.

### 1.3

This is a valid function represented as valid C, C++ and Java code.

### 1.4

This is not a function - it does something other than just returning a value.
This method reads from the system's time!

#### 1.5

This is not a function - it also does something other than just return a
value based on its inputs. This method prints to the console!

## 2. Haskell

### 2.1

People might need a bunch of help installing GHC. When people have a problem,
write it down and we can try to sort out common ones.

This is pretty important so should spend as long as necessary on this.

### 2.2

The goal is to get people used to evaluating, loading, reloading and
type-checking Haskell code.

### 2.3

Show the following parts of Haskell:

* Values
* Types (without polymorphism)
* Functions
* Operators
* Data types (without polymorphism)
* Pattern matching

And the various forms of syntactic sugar on the way.
