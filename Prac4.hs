{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE NoImplicitPrelude #-}

{-# OPTIONS_GHC -Wall #-}

module Prac4 where

import Data.String
import Data.Eq
import Data.Ord
import Data.Bool
import Data.Char
import Text.Show

import Prelude (error, (-), (+), (*), Integer, fromIntegral, seq, IO)

{- TYPES, CLASSES AND FUNCTIONS -}

data Optional a = Empty | Full a
  deriving (Eq, Show)

data List a = Nil | Cons a (List a)
  deriving (Eq, Show)

data Pair a b = Pair a b
  deriving (Eq, Show)

data NonEmptyList a = NonEmptyList a (List a)
  deriving (Eq, Show)

data ParseResult x =
  ParseError String | ParseSuccess x String
  deriving (Eq, Show)

data Parser x = Parser (String -> ParseResult x)

runParser :: Parser a -> String -> ParseResult a
runParser (Parser f) = f

foldRight :: (a -> b -> b) -> b -> List a -> b
foldRight _ b Nil      = b
foldRight f b (h `Cons` t) = f h (foldRight f b t)

foldLeft :: (b -> a -> b) -> b -> List a -> b
foldLeft _ b Nil      = b
foldLeft f b (h `Cons` t) = let b' = f b h in b' `seq` foldLeft f b' t


class Functor k where
  fmap :: (a -> b) -> k a -> k b

class (Functor k) => Applicative k where
  (<*>) :: k (a -> b) -> k a -> k b
  pure :: a -> k a
infixl 4 <*>

class (Applicative k) => Monad k where
  (>>=) :: k a -> (a -> k b) -> k b
infixl 1 >>=

id :: a -> a
id = \a -> a

const :: b -> a -> b
const b = \_ -> b

(<$>) :: (Functor k) => (a -> b) -> k a -> k b
(<$>) = fmap
infixl 4 <$>

(<$) :: (Functor k) => b -> k a -> k b
b <$ ka = fmap (const b) ka
infixl 4 <$

($>) :: (Functor k) => k a -> b -> k b
ka $> b = fmap (const b) ka
infixl 4 $>

-- | Run two applicative computations, discarding second result.
--   (Note, the <* "points to" the returned value)
(<*) :: (Applicative k) => k a -> k b -> k a
ka <* kb = const <$> ka <*> kb
infixl 4 <*

-- | Run two applicative computations, discarding first result.
--   (Note, the *> "points to" the returned value)
(*>) :: (Applicative k) => k a -> k b -> k b
ka *> kb = (\_ b -> b) <$> ka <*> kb
infixl 4 *>

liftA2 :: (Applicative k) => (a -> b -> c) -> k a -> k b -> k c
liftA2 f ka kb = f <$> ka <*> kb

instance Functor [] where
  fmap _ [] = []
  fmap f (x:xs) = f x : fmap f xs


{- EXERCISE 1: More Applicative and Monad derived operations -}

-- | "Sequence" means to turn a list of computations into a single
-- computation that collects the results as a list.
sequence :: (Applicative k) => List (k a) -> k (List a)
sequence = error "todo"

-- | Sequentially execute a list of computations, ignoring results
sequence_ :: (Applicative k) => List (k a) -> k ()
sequence_ = error "todo"

-- NOTE: where values have the same type, you can provide their
-- type signatures in a single declaration, as below for 'when'
-- and 'unless'.  I recommend against this, but you might see it
-- in the wild.

when, unless :: (Applicative k) => Bool -> k () -> k ()
when = error "todo"
unless = error "todo"

-- | Like (>>=), but reads right-to-left.
--   (Nice to use with /do/ syntax)
--
(=<<) :: (Monad k) => (a -> k b) -> k a -> k b
(=<<) = error "todo"
infixr 1 =<<

join :: (Monad k) => k (k a) -> k a
join = error "todo"


{- EXERCISE 2: Functor, Applicative, Monad for Parser

You might already have done this, but practice is essential
to internalising your understanding of these abstractions,
and how they apply to Parsers.

-}

instance Functor Parser where
  fmap = error "todo"

instance Applicative Parser where
  (<*>) = error "todo"
  pure = error "todo"

instance Monad Parser where
  (>>=) = error "todo"


{- EXERCISE 3: Parser building blocks ("combinators") -}

-- | Succeeds if string is non-empty and next Char satisfies
--   the predicate
satisfy :: (Char -> Bool) -> Parser Char
satisfy = error "todo"

-- | Parse a specific character
char :: Char -> Parser Char
char = error "todo"

-- | Always succeeds if the string is non-empty
anyChar :: Parser Char
anyChar = error "todo"

-- | Succeed iff all input has been consumed.
endOfInput :: Parser ()
endOfInput = error "todo"

-- | If the first parser fails, try the second parser
--
-- Often pronounced "or", i.e. "this or that"
--
(<|>) :: Parser a -> Parser a -> Parser a
(<|>) = error "todo"
infixl 3 <|>  -- NOTE, lower precendence than <$>, <*>, etc.

-- If the parser succeeds, return @Full r@, otherwise succeed
-- with @Empty@.
optional :: Parser a -> Parser (Optional a)
optional = error "todo"

-- | Run the parser as many times as possible;
--   collect list of results.
many :: Parser a -> Parser (List a)
many = error "todo"

-- | Run the parser as many times as possible;
--   must succeed at least once;
--   collect non-empty list of results.
some :: Parser a -> Parser (NonEmptyList a)
some = error "todo"

sepBy :: Parser a -> Parser sep -> Parser (List a)
sepBy = error "todo"


{- EXERCISE 4: NotJSON

NotJSON is a simplified version of JSON.
You will write a parser for it.

- Boolean representation is 't' or 'f'

- Number uses decimal representation (leading '-' for negative)

- String is '"' followed by zero or more characters, ending at
  the next occurrence of '"'.  The double quotes are not part
  of the string.  No escaping facility for '"'.

- Maps are delimited by '{' and '}', key-value pairs are
  separated by ',', keys and value are separated by ':',
  keys are strings.

- No whitespace (except inside strings)

Parser implementations for some of the data types have been
completed for you.

-}

data NJValue
  = NJNumber Integer
  | NJBool Bool
  | NJString String
  | NJMap (List (Pair String NJValue))
  deriving (Eq, Show)

parseNJNumber :: Parser Integer
parseNJNumber = parseSignedInteger

parseNJBool :: Parser Bool
parseNJBool = char 't' $> True <|> char 'f' $> False

parseNJString :: Parser String
parseNJString = error "todo"

parseNJMap :: Parser (List (Pair String NJValue))
parseNJMap = error "todo"

parseNotJSON :: Parser NJValue
parseNotJSON = error "todo"

-- | Parse a digit ('0' .. '9')
parseDigit :: Parser Integer
parseDigit = c2i <$> satisfy (\c -> c >= '0' && c <= '9')
  where
  c2i c = fromIntegral (ord c - ord '0')

-- | Parse non-signed integer
parseInteger :: Parser Integer
parseInteger = f <$> some parseDigit
  where
  f (NonEmptyList x xs) = foldLeft (\n -> (n * 10 +)) 0 (x `Cons` xs)

parseSignedInteger :: Parser Integer
parseSignedInteger =
  (char '-' $> ((-1) *)) <*> parseInteger
  <|> parseInteger


{- EXERCISE 5: NotJSON pretty printer function

NotJSON is not friendly to human eyes.  Write a pretty printer
program for NotJSON.  This is an open-ended exercise, but here are
some guidelines:

- Nested map structures should be printed with increasing indent

- Maps should be printed with key-value pairs on their own line(s)

- The program should read the name of the file(s) to pretty-print
  from program arguments (or read standard input if args empty).

-}

main :: IO ()
main = error "todo"
