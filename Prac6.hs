{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE UndecidableInstances #-}

{-# OPTIONS_GHC -Wall -Wno-unused-imports #-}

module Prac6 where

import Prelude (error, even, id, (.), ($))

import Control.Applicative (Applicative(..))
import Control.Monad (Monad(..))
import Data.Bool
import Data.Eq
import Data.Foldable (elem, foldr)
import Data.Functor
import Data.Ord
import Data.Int (Int)
import Data.String (String)
import Data.Tuple (fst, snd)
import Text.Show


{- ACKNOWLEDGEMENTS

The exercises in this prac are based on the NICTA/Data61/System F
FP course (https://github.com/system-f/fp-course) and in particular
the Course.Applicative and Course.StateT modules.

-}


{- PRELUDE -}

data Optional a = Empty | Full a
  deriving (Eq, Show)

instance Functor Optional where
  fmap f o = case o of
    Empty -> Empty
    Full a -> Full (f a)

instance Applicative Optional where
  pure = Full
  o1 <*> o2 = case o1 of
    Empty -> Empty
    Full f -> f <$> o2

instance Monad Optional where
  o >>= f = case o of
    Empty -> Empty
    Full a -> f a

when :: (Applicative k) => Bool -> k () -> k ()
when b k = case b of
  True -> k
  False -> pure ()


{- REVISION: Identity -}

data Identity a = Identity { getIdentity :: a }
  deriving (Show)

instance Functor Identity where
  fmap :: (a -> b) -> Identity a -> Identity b
  fmap = error "todo"

instance Applicative Identity where
  pure :: a -> Identity a
  pure = error "todo"

  (<*>) :: Identity (a -> b) -> Identity a -> Identity b
  (<*>) = error "todo"

instance Monad Identity where
  (>>=) :: Identity a -> (a -> Identity b) -> Identity b
  (>>=) = error "todo"


{- REVISION: Semigroup and Monoid -}

class Semigroup (a :: *) where
  (<>) :: a -> a -> a

class (Semigroup a) => Monoid a where
  mempty :: a

instance Semigroup ([] a) where
  l1 <> l2 = foldr (:) l2 l1

instance Monoid ([] a) where
  mempty = []


{- EXERCISE: Writer -}

-- Intuition: /computing/ a value of type 'a'
--   while /accumulating/ a value of type 'w'
--
-- e.g.
--
-- * count line numbers while parsing a file
-- * log messages while performing a computation
--
data Writer w a = Writer w a
  deriving (Show)

instance Functor (Writer w) where
  fmap :: (a -> b) -> Writer w a -> Writer w b
  fmap = error "todo"

instance Applicative (Writer w) where
  pure :: a -> Writer w a
  pure = error "todo"

  (<*>) :: Writer w (a -> b) -> Writer w a -> Writer w b
  (<*>) = error "todo"

instance Monad (Writer w) where
  (>>=) :: Writer w a -> (a -> Writer w b) -> Writer w b
  (>>=) = error "todo"

tell :: w -> Writer w ()
tell = error "todo"

{- Thought exercise:

* How is Writer similar to Const (from Prac 5)?
* How do they differ?

-}


{- EXERCISE: StateT ; stateful computations

You have already seen StateT in the lectures.
Let's go over it again.

-}

-- | USEFUL FUNCTION: Apply f to /first/ value in tuple
first :: (a -> b) -> (a, c) -> (b, c)
first = error "todo"

data StateT s k a =
  StateT { runStateT :: s -> k (a, s) }

instance (Functor k) => Functor (StateT s k) where
  fmap :: (a -> b) -> StateT s k a -> StateT s k b
  fmap = error "todo"

instance (Monad k) => Applicative (StateT s k) where
  pure :: a -> StateT s k a
  pure = error "todo"

  (<*>) :: StateT s k (a -> b) -> StateT s k a -> StateT s k b
  (<*>) = error "todo"

instance Monad k => Monad (StateT s k) where
  (>>=) :: StateT s k a -> (a -> StateT s k b) -> StateT s k b
  (>>=) = error "todo"

-- | Run the StateT computation, returning the /final state/
execStateT :: (Functor k) => StateT s k a -> s -> k s
execStateT = error "todo"

-- | Run the StateT computation, returning the /result value/
evalStateT :: (Functor k) => StateT s k a -> s -> k a
evalStateT = error "todo"

-- | Get the current state
get :: (Applicative k) => StateT s k s
get = error "todo"

-- | Put (set) a new state
put :: (Applicative k) => s -> StateT s k ()
put = error "todo"

-- Observe that we recover a "pure" state computation
-- by instantiating 'k' at the "neutral" monad, i.e.
-- 'Identity'
type State s = StateT s Identity

execState :: State s a -> s -> s
execState = error "todo"

evalState :: State s a -> s -> a
evalState = error "todo"

-- | Filter a list with an "effectful" predicate
filtering :: (Applicative k) => (a -> k Bool) -> [a] -> k [a]
filtering = error "todo"

-- | Remove all duplicate elements in a list.
--
-- /Tip:/ Use `filtering` and `State [a]`.
--
distinct :: (Eq a) => [a] -> [a]
distinct = error "todo"

-- | Remove all duplicate elements in a list.
-- However, if you see a value greater than `100` in the list,
-- abort the computation by producing `Empty`.
--
-- /Tip:/ Use `filtering` and `StateT [Int] Optional`
--
-- >>> distinctMax100 [1,2,3,2,1]
-- Full [1,2,3]
--
-- >>> distinctMax100 [1,2,3,2,1,101]
-- Empty
--
distinctMax100 :: [Int] -> Optional [Int]
distinctMax100 = error "todo"


{- EXERCISE: OptionalT ; computations that can "fail" -}

data OptionalT k a =
  OptionalT { runOptionalT :: k (Optional a) }

instance (Functor k) => Functor (OptionalT k) where
  fmap :: (a -> b) -> OptionalT k a -> OptionalT k b
  fmap = error "todo"

instance (Monad k) => Applicative (OptionalT k) where
  pure :: a -> OptionalT k a
  pure = error "todo"

  (<*>) :: OptionalT k (a -> b) -> OptionalT k a -> OptionalT k b
  (<*>) = error "todo"

instance (Monad k) => Monad (OptionalT k) where
  (>>=) :: OptionalT k a -> (a -> OptionalT k b) -> OptionalT k b
  (>>=) = error "todo"


-- | Remove all duplicate integers from a list. Produce a log as you
-- go.  If there is an element above 100, then abort the entire
-- computation and produce no result, but **always keep a log**.  If
-- you abort the computation, produce a log with the value,
-- "aborting > 100: " followed by the value that caused it.  If you
-- see an even number, produce a log message, "even number: "
-- followed by the even number.  Other numbers produce no log
-- message.
--
-- /Tip:/ Use `filtering` and `StateT [Int] (OptionalT (Writer [String]))`
--
-- >>> distinctMax100WithLog [1,2,3,2,6]
-- Writer ["even number: 2","even number: 2","even number: 6"] (Full [1,2,3,6])
--
-- >>> distinctMax100WithLog [1,2,3,2,6,106]
-- Writer ["even number: 2","even number: 2","even number: 6","aborting > 100: 106"] Empty
distinctMax100WithLog :: [Int] -> Writer [String] (Optional [Int])
distinctMax100WithLog = error "todo"


{- EXERCISE: "lifting" functions

Implement the following functions, then use them to refactor
'distinctMax100WithLog' above.

Neither of the two approaches - explicit constructors versus lifting
functions - is necessarily better or worse.  The approaches may even
be mixed.  Use whichever approach you prefer.

-}

-- lift a computation into the StateT
liftS :: (Functor k) => k a -> StateT s k a
liftS = error "todo"

-- lift a computation into an OptionalT
liftO :: (Functor k) => k a -> OptionalT k a
liftO = error "todo"

-- lift a computation into an /Empty/ OptionalT (failure)
failO :: (Functor k) => k a -> OptionalT k b
failO = error "todo"


{- EXERCISE: WriterT

Writer has a monad transformer.

Note the isomorphism between 'Writer w a' (as initially defined
above) and '(a, w)'.  The definition of 'WriterT' uses the latter
representation.

-}

data WriterT w k a = WriterT { runWriterT :: k (a, w) }

instance (Show (k (a, w))) => Show (WriterT w k a) where
  show (WriterT k) = show k

instance (Functor k) => Functor (WriterT w k) where
  fmap :: (a -> b) -> WriterT w k a -> WriterT w k b
  fmap = error "todo"

instance (Monoid w, Applicative k) => Applicative (WriterT w k) where
  pure :: a -> WriterT w k a
  pure = error "todo"

  (<*>) :: WriterT w k (a -> b) -> WriterT w k a -> WriterT w k b
  (<*>) = error "todo"

instance (Monoid w, Monad k) => Monad (WriterT w k) where
  (>>=) :: WriterT w k a -> (a -> WriterT w k b) -> WriterT w k b
  (>>=) = error "todo"

tell' :: (Applicative k) => w -> WriterT w k ()
tell' = error "todo"
