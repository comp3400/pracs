{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE RankNTypes #-}

{-# OPTIONS_GHC -Wall -Wno-unused-imports #-}

module Prac8 where

import Prelude ((.), ($), const, id, flip, error, Integer)

import Control.Applicative (Applicative(..))
import Data.Bool
import Data.Eq
import Data.Foldable
import Data.Functor
import Data.Functor.Const
import Data.List (reverse)
import Data.Maybe
import Data.Monoid (Monoid(..), First(..))
import Data.Semigroup (Semigroup(..))
import Data.Ord
import Data.String (String)
import Data.Traversable
import Text.Show


{- PRELUDE -}

newtype Identity a = Identity { getIdentity :: a }
  deriving (Show)

instance Functor Identity where
  fmap :: (a -> b) -> Identity a -> Identity b
  fmap f (Identity a) = Identity (f a)

instance Applicative Identity where
  pure :: a -> Identity a
  pure = Identity

  (<*>) :: Identity (a -> b) -> Identity a -> Identity b
  Identity f <*> Identity a = Identity (f a)

data List a = Nil | Cons a (List a)
  deriving (Eq, Show)
infixr 5 `Cons`

foldRight :: (a -> b -> b) -> b -> List a -> b
foldRight _ b Nil      = b
foldRight f b (h `Cons` t) = f h (foldRight f b t)


{- REVIEW: Traversal -}

type Traversal s t a b =
  forall k. (Applicative k)
  => (a -> k b) -> s -> k t

type Traversal' s a = Traversal s s a a

-- | Traversal for **both sides** of a pair
both :: Traversal (a, a) (b, b) a b
both = error "todo"

-- | A Traversal over any 'Traversable t => t a'
traversed :: (Traversable t) => Traversal (t a) (t b) a b
traversed = error "todo"

-- | Obtain a Traversal that can be composed with
-- another optic to "filter" it.
--
-- This is not a lawful Traversal in general.
-- You must take care not to invalidate the predicate!
--
filtered :: (a -> Bool) -> Traversal a a a a
filtered = error "todo"


{- REVIEW: Setter -}

type Setter s t a b = (a -> Identity b) -> s -> Identity t

-- | Modify the target(s) of a traversal
over :: Setter s t a b -> (a -> b) -> s -> t
over = error "todo"

-- | Set the target(s) of a traversal
set :: Setter s t a b -> b -> s -> t
set = error "todo"

{- REVIEW: Fold -}

type Fold s a =
  forall r. (Monoid r)
  => (a -> Const r a) -> s -> Const r s

-- | A Fold over any 'Foldable t => t a'
folded :: (Foldable t) => Fold (t a) a
folded = error "todo"

-- If we specialise foldMapOf's functor to 'Const r' we can
-- avoid the Monoid constraint here
foldMapOf
  :: ((a -> Const r b) -> s -> Const r t)
  -> (a -> r)
  -> s
  -> r
foldMapOf = error "todo"

-- We can extract this type to a type synonym:
type Getting r s a = (a -> Const r a) -> s -> Const r s

foldOf :: Getting a s a -> s -> a
foldOf = error "todo"

firstOf :: Getting (First a) s a -> s -> Maybe a
firstOf = error "todo"


{- REVIEW: Lens -}

type Lens s t a b =
  forall k. (Functor k)
  => (a -> k b) -> s -> k t

type Lens' s a = Lens s s a a

fstL :: Lens (a, x) (b, x) a b
fstL = error "todo"

sndL :: Lens (x, a) (x, b) a b
sndL = error "todo"

view :: Getting a s a -> s -> a
view = foldOf


{- EXERCISE: Endo -}

newtype Endo a = Endo { appEndo :: a -> a }

instance Semigroup (Endo a) where
  (<>) = error "todo"

instance Monoid (Endo a) where
  mempty = error "todo"

foldrOf
  :: Getting (Endo r) s a
  -> (a -> r -> r)
  -> r
  -> s
  -> r
foldrOf = error "todo"

toListOf :: Getting (Endo [a]) s a -> s -> [a]
toListOf = error "todo"


{- EXERCISE: Profunctor -}

-- | Map contravariantly on the first type parameter,
--   and covariantly on the second type parameter.
class Profunctor (p :: * -> * -> *) where
  dimap :: (b -> a) -> (c -> d) -> p a c -> p b d

instance Profunctor (->) where
  dimap :: (b -> a) -> (c -> d) -> (a -> c) -> (b -> d)
  dimap = error "todo"


newtype Tagged x a = Tagged { unTagged :: a }

instance Functor (Tagged x) where
  fmap :: (a -> b) -> Tagged x a -> Tagged x b
  fmap = error "todo"

instance Applicative (Tagged x) where
  pure :: a -> Tagged x a
  pure = error "todo"

  (<*>) :: Tagged x (a -> b) -> Tagged x a -> Tagged x b
  (<*>) = error "todo"

instance Profunctor Tagged where
  dimap :: (b -> a) -> (c -> d) -> Tagged a c -> Tagged b d
  dimap = error "todo"


{- EXERCISE: Choice -}

data Either a b = Left a | Right b
  deriving (Eq, Show)

-- "inline" case analysis for 'Either'
either :: (a -> r) -> (b -> r) -> Either a b -> r
either = error "todo"

-- | Turn a "plain" profunctor into a profunctor that
-- maps the left or right side of an Either
class (Profunctor p) => Choice p where
  left  :: p a b -> p (Either a c) (Either b c)
  right :: p a b -> p (Either c a) (Either c b)

instance Choice (->) where
  left :: (a -> b) -> (Either a c -> Either b c)
  left = error "todo"

  right :: (a -> b) -> (Either c a -> Either c b)
  right = error "todo"

instance Choice Tagged where
  left :: Tagged a b -> Tagged (Either a c) (Either b c)
  left = error "todo"

  right :: Tagged a b -> Tagged (Either c a) (Either c b)
  right = error "todo"


{- EXERCISE: Prism -}

-- | Prism generalises Traversal,
-- replacing '(->)' with '(Choice p) => p'
--
type Prism s t a b =
  forall p k. (Choice p, Applicative k)
  => p a (k b) -> p s (k t)

type Prism' s a = Prism s s a a

-- | Prism to Left constructor
_Left :: Prism (Either a x) (Either b x) a b
_Left = error "todo"

-- | Prism to Left constructor
_Right :: Prism (Either x a) (Either x b) a b
_Right = error "todo"

-- Construct a prism from a constructor and "partial"
-- getter
prism :: (b -> t) -> (s -> Either t a) -> Prism s t a b
prism = error "todo"

-- | Prism to Just constructor
_Just :: Prism (Maybe a) (Maybe b) a b
_Just = error "todo"

-- | Prism to Nothing constructor
_Nothing :: Prism (Maybe a) (Maybe a) () ()
_Nothing = error "todo"

{- Composing Prisms

What is the type of a Prism composed with...

* another Prism?
* a Lens?
* a Traversal?
* a Fold?

Use GHCi to check.  Why does each composition have the
type it does?

-}

{- EXERCISE: Iso[morphism] -}

-- | We can relax Choice to Profunctor, and Applicative
--   to Functor, we get an Isomorphism
type Iso s t a b =
  forall p k. (Profunctor p, Functor k)
  => p a (k b) -> p s (k t)

type Iso' s a = Iso s s a a

-- | An Iso is a witness of structural isomorphism
iso :: (s -> a) -> (b -> t) -> Iso s t a b
iso = error "todo"

swapped :: Iso' (a, b) (b, a)
swapped = error "todo"

reversed :: Iso' [a] [a]
reversed = error "todo"

listIso :: Iso' [a] (List a)
listIso = error "todo"

-- | Use a Prism (or Iso) as a constructor for the
-- "large" type
--
-- **Hint** use 'Tagged' and 'Identity'
--
review :: Prism s t a b -> b -> t
review _ = error "todo"

-- | Invert an Iso
from :: Iso s t a b -> Iso b a t s
from _ = error "todo"

{- Composing Prisms

What is the type of an Iso composed with...

* another Iso?
* a Prism?
* a Traversal?
* a Lens?
* a Fold?

Use GHCi to check.  Why does each composition have the
type it does?

-}

-- | Iso that reverses a 'List a'
reversedList :: Iso' (List a) (List a)
reversedList = error "todo"


{- EXERCISE: Application -}

type Name = String

type Address = String

data Person = Person Name Address
  deriving (Show)

personName :: Lens' Person Name
personName = error "todo"

personAddress :: Lens' Person Address
personAddress = error "todo"

type ABN = String

type RegistrationNumber = String

data RegisteredTo
  = RegisteredToBusiness ABN
  | RegisteredToPerson Person
  deriving (Show)

regoABN :: Prism' RegisteredTo ABN
regoABN = error "todo"

regoPerson :: Prism' RegisteredTo Person
regoPerson = error "todo"

type Registration = (RegistrationNumber, RegisteredTo)

type VIN = Integer

type Make = String
type Model = String

data Vehicle = Vehicle
  VIN
  Make
  Model
  (Maybe Registration)
  deriving (Show)

vehicleVIN :: Lens' Vehicle VIN
vehicleVIN = error "todo"

vehicleMake :: Lens' Vehicle Make
vehicleMake = error "todo"

vehicleModel :: Lens' Vehicle Model
vehicleModel = error "todo"

vehicleRegistration :: Lens' Vehicle (Maybe Registration)
vehicleRegistration = error "todo"

db :: [Vehicle]
db =
  [ Vehicle 1 "Lamborghini" "Sian"
      (Just ("725MJM", RegisteredToPerson alice))
  , Vehicle 2 "Ford" "Pinto"
      (Just ("692LXO", RegisteredToPerson bob))
  , Vehicle 3 "Ford" "Model T"
      (Just ("755XXH", RegisteredToBusiness "42"))
  , Vehicle 4 "Ford" "Pinto"
      Nothing
  , Vehicle 5 "Lada" "Niva"
      (Just ("363OCL", RegisteredToPerson carol))
  , Vehicle 6 "McLaren" "Senna"
      (Just ("687QYF", RegisteredToPerson alice))
  ]
  where
    alice = Person "Alice" "208 Vale Drive"
    bob = Person "Bob" "64 Brook Drive"
    carol = Person "Carol" "292 Galvin St"

-- | Register vehicle with given VIN, using the
-- specified RegistrationNumber.  If VIN is unknown
-- do nothing.
--
register
  :: VIN
  -> RegisteredTo
  -> RegistrationNumber
  -> [Vehicle]
  -> [Vehicle]
register = error "todo"

-- | Fraser has become the proud owner of a classic
-- Ford Pinto, VIN 4.
registerFraser'sPinto :: [Vehicle] -> [Vehicle]
registerFraser'sPinto =
  error "todo"
  where
    --nextRego = "757AQA"
    --fraser = Person "Fraser" "7 Pierce Road"

-- | Uh oh, Ford Pintos are exploding.
--
-- We need to find the address of all registered owners
-- of Pintos, so we can send them some paperwork.
--
-- Don't worry about the vehicles registered to
-- companies.
--
pintoPeople :: [Vehicle] -> [Address]
pintoPeople = error "todo"
