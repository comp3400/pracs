# COMP3400 - Prac #4

Last prac covered:

- The functor > applicative > monad abstraction heirarchy
- Implementing instances for several foundational data types
- Parser exercises

This week's lecture covered:

- `Functor` and `Applicative`
- Automated specification-based testing
- The `IO` datatype and its instances

This prac covers:

- More `Applicative` and `Monad` derived operations
- Parser building-blocks (*combinators*), applied to contruct a
  non-trivial parser.
- Practical `IO` (file I/O, pretty-printing)

Always remember:

- There is no substitute for practice
- FP can be challenging; we are here to help


## Isomorphic types

We say a type is *isomorphic* to another type if it has the same
structure.  For example, the following types are isomorphic:

```haskell
data Optional a = Empty   | Full a
data Maybe a    = Nothing | Just a
```

The algebra of type isomorphism goes beyond mere substitution of
type and constructor names.  For example, `Optional (NonEmptyList
a)` is isomorphic to `List a`; the `Empty` constructor corresponds
to the `Nil` case, and the `Full (NonEmptyList x xs)` case
corresponds to the `Cons x xs` case.

Intuitively, types are isomorphic if you can convert *in both
directions* without losing information.

Concretely, types `a` and `b` are isomorphic if there exist
functions `f :: a -> b` and `g :: b -> a` such that `g (f a) = a`
and `f (g b) = b`, i.e. the composition of f and g is the *identity*
function.


## Isomorphic types in *base* library

Many of the foundational types you have learned are isomorphic to
counterparts in Haskell's *base* library.  You may wish to begin
using the types from *base*.  An advantage to using the types from
*base* is that you can use the type class instances, derived
functions and other library code provided by *base*.  But feel free
to use whichever variation you are more comfortable with.

Types with counterparts in *base* include:

```haskell
data Optional a = Empty   | Full a
data Maybe    a = Nothing | Just a

data List a = Nil | Cons a (List a)
data []   a = []  | a : [a]

data Pair a b = Pair a b
data (,)  a b = (,) a b
-- There is special syntax for constructing tuples:
-- (,) 'a' 1 == ('a', 1)

data Or     a b = IsA  a | IsB   b
data Either a b = Left a | Right b

data Unit = Unit
data ()   = ()

data NonEmptyList = NonEmptyList a (List a)
data NonEmpty     = a :| [a]    -- import Data.List.NonEmpty
```


## Exercises

The goals of this week's exercises are:

- Reinforcement of `Functor`, `Applicative`, `Monad` abstraction.
- Introduce some more derived operations
- Apply what you've learned to build a non-trivial parser.
- Introduce practical `IO` and `do` notation.

There's not much else to say; open `Prac4.hs` and get started.  If
you get stuck ask for help (`#comp3400` or email) and move on to
other problems in the meantime.
